[![build status](https://gitlab.com/damianmullins/damianmullins.gitlab.io/badges/gitlab/build.svg)](https://gitlab.com/damianmullins/damianmullins.gitlab.io/commits/gitlab)

# [damianmullins.gitlab.io](http://damianmullins.gitlab.io)

Files for my personal website [damianmullins.gitlab.io](http://damianmullins.gitlab.io).
