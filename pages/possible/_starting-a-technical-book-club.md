---
title: Starting a technical book club
date: "2016-01-01T01:01"
path: "/starting-a-technical-book-club/"
draft: true
tags:
  - tech
  - books
  - book club
---

## Starting a book club
- As a book club noob

## Reasons
- Since planning my year ahead, book club seemed like a good way to ensure I remain motivated.
- Always wanted to join a book club but never had the opportunity or courage to start one myself.

## Getting a group together
- Sending out email

## Forming a plan
- Once you know people are interested..
- How many people are interested?

## Choosing a book
- Add some initial suggestions which cover a variety of topics

## Tracking choices
- Decide on a format to list suggested books
- I used Trello - **TODO: add screenshots of my board**

## Breaking the book down

## Planning discussions

## Planning activities
