---
title: Internet Explorer is dying
date: "2016-01-19T15:45"
path: "/internet-explorer-is-dying/"
draft: true
tags:
  - thoughts
  - browser
  - internet explorer
  - microsoft
---

So Microsoft has dropped support for Internet Explorer 8, 9 and 10 as of 12th January, it will be interesting to see how much of an effect this will have on web traffic; will more users begin to upgrade to IE 11 or Edge? Or move away and start using another.

In a way I'll be sad to see these old browsers die off, I've put _so_ many hours into coding workarounds for all sorts of bugs in each version over the last 7 years ([IE 8 was released March 19 2009](https://en.wikipedia.org/wiki/Internet_Explorer_8)!).

In looking up IE release dates I've just realised I started building websites around the time IE 5 was released ([1999](https://en.wikipedia.org/wiki/Internet_Explorer_5)), ahhh nostalgia.. and a sense of impending old age!
